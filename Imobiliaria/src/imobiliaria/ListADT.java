/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

import java.util.Iterator;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public interface ListADT<T> extends Iterable<T> {

    /**
     *
     * Método para remover um elemento da lista
     *
     * @return Devolve o elemento removido
     * @throws EmptyCollectionException caso a lista não contenha elementos
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    public T remove(T element) throws EmptyCollectionException, ElementNotFoundException;

    /**
     *
     * Método para verificar se um elemento existe na lista
     *
     * @return Devolve um boolean no qual será true caso o elemento exista
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    public boolean contains(T target) throws ElementNotFoundException;

    /**
     *
     * Método para ver se a lista está vazia
     *
     * @return Devolve um boolean que será true caso a lista esteja vazia
     */
    public boolean isEmpty();

    /**
     *
     * Método para returnar o tamanho da lista
     *
     * @return Devolve o tamanho da lista
     */
    public int size();

    /**
     *
     * Método para usar o iterador
     *
     * @return Devolve um iterador
     */
    @Override
    public Iterator<T> iterator();

    /**
     *
     * Método para apresentar uma string com todos os elementos da lista
     *
     * @return Devolve uma string com os elementos da lista
     */
    @Override
    public String toString();

    /**
     *
     * Método para encontrar um elemento na lista
     *
     * @return Devolve o elemento caso exista
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    public T find(T target) throws ElementNotFoundException;

}

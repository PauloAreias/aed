/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

import java.io.Serializable;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public class OrderedLinkedList<T> extends LinkedList<T> implements OrderedListADT<T>, Serializable{

    /**
     * 
     * Método para adicionar elemento à lista de modo ordenado
     * 
     * @throws UncomparableException caso não seja possível comparar os elementos
     */
    @Override
    public void add(T element) throws UncomparableException {
        LinkedNode<T> newElement = new LinkedNode<>(element);
        
        if (!(element instanceof Comparable)) {
            throw new UncomparableException("Erro na linked list");
        }

        if (head == null) {
            head = newElement;
        } else {
            Comparable<T> comparable = (Comparable<T>) element;
            LinkedNode<T> previous = null;
            LinkedNode<T> current = head;
            
            while (current != null && comparable.compareTo(current.getElement()) > 0) {
                previous = current;
                current = current.getNext();
            }

            if (current == head) {
                newElement.setNext(head);
                head = newElement;
            } else if (current == null) {
                previous.setNext(newElement);
                newElement.setNext(null);

            } else {
                previous.setNext(newElement);
                newElement.setNext(current);
            }
        }
        count++;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

import java.io.Serializable;

/**
 *
 * @author Pedro Marques
 */
public class Cliente implements Comparable, Serializable {

    private String nome;
    private String email;
    private String contacto;
    private double desconto = 1;

    /**
     * 
     * Construtor
     * 
     */
    public Cliente() {
    }

    /**
     * 
     * Construtor
     * 
     */
    public Cliente(String nome, String email, String contacto) {
        this.nome = nome;
        this.email = email;
        this.contacto = contacto;
    }

    /**
     * 
     * Método get para receber o nome
     * 
     */
    public String getNome() {
        return nome;
    }

    /**
     * 
     * Método set para alterar o nome
     * 
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * 
     * Método get para receber o email
     * 
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * Método set para alterar o email
     * 
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * Método get para receber o contacto do cliente
     * 
     */
    public String getContacto() {
        return contacto;
    }

    /**
     * 
     * Método set para alterar o contacto
     * 
     */
    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    /**
     * 
     * Método get para receber o desconto
     * 
     */
    public double getDesconto() {
        return desconto;
    }

    /**
     * 
     * Método set para alterar o desconto
     * 
     */
    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    /**
     * 
     * Método compare para comparar 2 elementos
     * @return Devolve 0 se os clientes forem iguais
     * 
     */
    @Override
    public int compareTo(Object object) {
        int result;
        if (nome.equals(((Cliente) object).nome)) {
            if (email.equals(((Cliente) object).email)) {
                result = contacto.compareTo(((Cliente) object).contacto);
            } else {
                result = email.compareTo(((Cliente) object).email);
            }
        } else {
            result = nome.compareTo(((Cliente) object).nome);
        }

        return result;
    }

    /**
     * 
     * Método equals para comparar 2 elementos
     * @return Devolve true se os elementos forem iguais 
     * 
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Cliente)) {
            return false;
        }
        if ((Cliente) object == this) {
            return true;
        }

        Cliente cliente = (Cliente) object;
        if (!cliente.nome.equals(this.nome)) {
            return false;
        }
        if (!cliente.email.equals(this.email)) {
            return false;
        }
        if (!cliente.contacto.equals(this.contacto)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "\nCliente: " + "\nNome = " + nome + ";\nEmail = " + email + ";\nContacto = " + contacto + ";\nDesconto:" + (int) (100 - desconto * 100) + "%";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public interface OrderedListADT<T> extends ListADT<T> {
    
    /**
     * 
     * Método para adicionar um elemento à lista de modo ordenado
     *
     */
    public void add(T element) throws UncomparableException;
    
    /**
     * 
     * Método para criar uma string com todos os elementos da lista
     *
     * @return Retorna a string da lista
     */
    public String toString();
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Iterator;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public class LinkedList<T> implements ListADT<T>, Iterable<T> {

    protected LinkedNode<T> head;
    protected int count;

    /**
     *
     * Construtor
     *
     */
    public LinkedList() {
        head = null;
        count = 0;
    }

    /**
     *
     * Método para remover um elemento da lista
     *
     * @return Devolve o elemento removido
     * @throws EmptyCollectionException caso a lista não contenha elementos
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    @Override
    public T remove(T element) throws EmptyCollectionException, ElementNotFoundException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Erro na linked list.");
        }
        boolean found = false;
        LinkedNode<T> current = head;
        LinkedNode<T> previous = null;

        while (!found && current != null) {
            if (element.equals(current.getElement())) {
                found = true;
            } else {
                previous = current;
                current = current.getNext();
            }
        }
        if (!found) {
            throw new ElementNotFoundException("Erro na linked list.");
        }

        if (size() == 1) {
            head = null;
        } else if (current.equals(head)) {
            head = current.getNext();
            current.setNext(null);
        } else {
            previous.setNext(current.getNext());
            current.setNext(null);
        }
        count--;

        return current.getElement();

    }

    /**
     *
     * Método para armazenar os dados num ficheiro
     *
     */
    public void saveData(String fileName) {
        String fullFileName = fileName + ".txt";

        try {
            LinkedNode<T> current = head;

            FileOutputStream f = new FileOutputStream(new File(fullFileName));
            ObjectOutputStream o = new ObjectOutputStream(f);

            while (current != null) {
                o.writeObject(current.getElement());
                current = current.getNext();
            }

            o.close();
            f.close();

            System.out.println("Dados guardados com sucesso.");
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado.");
        } catch (IOException e) {
            System.out.println("Erro ao iniciar a stream.");
        }
    }

    /**
     *
     * Método para verificar se um elemento existe na lista
     *
     * @return Devolve um boolean no qual será true caso o elemento exista
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    @Override
    public boolean contains(T target) throws ElementNotFoundException {
        return find(target) != null;
    }

    /**
     *
     * Método para encontrar um elemento na lista
     *
     * @return Devolve o elemento caso exista
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    @Override
    public T find(T target) throws ElementNotFoundException {
        return SearchingLinkedList.linearSearch(head, target);
    }

    /**
     *
     * Método para encontrar um elemento na lista por ID
     *
     * @return Devolve o elemento caso exista
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    public T findImovelById(int id) throws ElementNotFoundException {
        return SearchingLinkedList.linearSearchImovelById(head, id);
    }

    /**
     *
     * Método para encontrar um elemento na lista por email
     *
     * @return Devolve o elemento caso exista
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    public T findClienteByEmail(String str) throws ElementNotFoundException {
        return SearchingLinkedList.linearSearchClienteByEmail(head, str);
    }

    /**
     *
     * Método para encontrar um elemento na lista por ID
     *
     * @return Devolve o elemento caso exista
     * @throws ElementNotFoundException caso o elemento introduzido não exista
     * na lista
     */
    public T findVisitaById(int id) throws ElementNotFoundException {
        return SearchingLinkedList.linearSearchVisitaById(head, id);
    }

    public String[] getArrayEmailCliente(int size) throws ElementNotFoundException {
        return SearchingLinkedList.linearSearchGetArrayEmailCliente(head, size);
    }

    /**
     *
     * Método para ver se a lista está vazia
     *
     * @return Devolve um boolean que será true caso a lista esteja vazia
     */
    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * Método para returnar o tamanho da lista
     *
     * @return Devolve o tamanho da lista
     */
    @Override
    public int size() {
        return count;
    }

    /**
     *
     * Método para usar o iterador
     *
     * @return Devolve um iterador
     */
    @Override
    public Iterator<T> iterator() {
        return new BasicIterator<>(head);
    }

    /**
     *
     * Método para apresentar uma string com todos os elementos da lista
     *
     * @return Devolve uma string com os elementos da lista
     */
    @Override
    public String toString() {
        String string = "";
        LinkedNode<T> current = head;

        string += current.getElement().toString() + " " + "\n";
        while (current.getNext() != null) {
            current = current.getNext();
            string += current.getElement().toString() + " " + "\n";
        }

        return string;
    }
}

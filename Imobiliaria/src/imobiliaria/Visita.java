package imobiliaria;

import java.io.Serializable;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public class Visita implements Comparable, Serializable {

    private int id;
    private int id_Imovel;
    private String email_Cliente;

    /**
     *
     * Construtor
     *
     */
    public Visita() {

    }

    /**
     *
     * Construtor
     *
     */
    public Visita(int i, int id_I, String email_C) {
        id = i;
        id_Imovel = id_I;
        email_Cliente = email_C;
    }

    /**
     *
     * Método get para receber o id
     *
     */
    public int getId() {
        return id;
    }

    /**
     *
     * Método set para alterar o id
     *
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * Método get para receber o id do imóvel
     *
     */
    public int getId_Imovel() {
        return id_Imovel;
    }

    /**
     *
     * Método set para alterar o id do imóvel
     *
     */
    public void setId_Imovel(int id_Imovel) {
        this.id_Imovel = id_Imovel;
    }

    /**
     *
     * Método get para receber o email do cliente
     *
     */
    public String getEmail_Cliente() {
        return email_Cliente;
    }

    /**
     *
     * Método set para alterar o email do cliente
     *
     */
    public void setEmail_Cliente(String email_Cliente) {
        this.email_Cliente = email_Cliente;
    }

    /**
     *
     * Método compara para comparar os elementos
     * @return Devolve 0 se os elementos forem iguais
     *
     */
    @Override
    public int compareTo(Object object) {
        return id - (((Visita) object).id);
    }

    /**
     *
     * Método equals para comparar 2 elementos
     * @return Devolve true caso os clientes sejam iguais
     *
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Visita)) {
            return false;
        }
        if ((Visita) object == this) {
            return true;
        }

        Visita visita = (Visita) object;
        if (visita.id != this.id) {
            return false;
        }
        if (visita.id_Imovel != this.id_Imovel) {
            return false;
        }
        if (!visita.email_Cliente.equals(this.email_Cliente)) {
            return false;
        }
        return true;
    }

    /**
     *
     * Método para apresentar os dados da visita
     *
     */
    @Override
    public String toString() {
        return "Visita: \nID da Visita: " + id + "\nReferencia do Imovel: " + id_Imovel + ";\nContacto Cliente: " + email_Cliente;
    }
}

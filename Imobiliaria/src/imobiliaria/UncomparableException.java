/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
class UncomparableException extends Exception {
    
    public UncomparableException(String collection) {
        super("O elemento não foi encontrado. " + collection);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public class Funcionalidades {

    /**
     * 
     * Variáveis OrderedLinkedList para criar listas de imóveis e clientes
     * 
     */
    private OrderedLinkedList<Imovel> imovelFunc;
    private OrderedLinkedList<Cliente> clienteFunc;

    private int id_Imovel = 0;

    /**
     *
     * Construtor
     *
     */
    public Funcionalidades() {
        imovelFunc = new OrderedLinkedList<>();
        clienteFunc = new OrderedLinkedList<>();
    }

    /**
     *
     * Método para adicionar imóvel
     *
     * @throws UncomparableException
     */
    public void adicionarImovel(String tipo, String localidade, double preco) throws UncomparableException {
        Imovel imovelRecebido = new Imovel(id_Imovel++, tipo, localidade, preco);
        imovelFunc.add(imovelRecebido);
    }

    /**
     *
     * Método para adicionar cliente
     *
     * @throws UncomparableException
     */
    public void adicionarCliente(Cliente clienteRecebido) throws UncomparableException {
        clienteFunc.add(clienteRecebido);
    }

    /**
     *
     * Método para remover o imóvel
     *
     * @throws EmptyCollectionException, ElementNotFoundException
     */
    public void removerImovel(Imovel imovelRecebido) throws EmptyCollectionException, ElementNotFoundException {
        Imovel imovel = imovelFunc.find(imovelRecebido);
        imovelFunc.remove(imovel);
    }

    /**
     *
     * Método para remover o cliente
     *
     * @throws EmptyCollectionException, ElementNotFoundException
     */
    public void removerCliente(Cliente clienteRecebido) throws EmptyCollectionException, ElementNotFoundException {
        Cliente cliente = clienteFunc.find(clienteRecebido);
        clienteFunc.remove(cliente);
    }

    /**
     *
     * Método para verificar os dados de todos os imóveis existentes
     *
     */
    public void verificarImoveis() {
        System.out.println(imovelFunc.toString());
    }

    /**
     *
     * Método para verificar os dados de todos os clientes existentes
     *
     */
    public void verificarClientes() {
        System.out.println(clienteFunc.toString());
    }

    private Cliente[] getClientes(String[] email) throws ElementNotFoundException {

        int i;
        Cliente[] clientes = new Cliente[email.length];

        for (i = 0; i < email.length; i++) {
            clientes[i] = clienteFunc.findClienteByEmail(email[i]);
        }
        return clientes;
    }

    /**
     *
     * Método usado para procurar pelo imóvel
     *
     * @throws ElementNotFoundException
     */
    public void procurarImovel(Imovel imovelRecebido) throws ElementNotFoundException {
        boolean contains = imovelFunc.contains(imovelRecebido);

        if (contains) {
            Imovel imovel = imovelFunc.find(imovelRecebido);
            System.out.println(imovel.toString());
            if (imovel.getVisitasSize() == 0) {
                System.out.println("Imovel sem visitas.");
            } else {
                System.out.println("Clientes visitantes: " + Arrays.toString(getClientes(imovel.getArrayEmailCliente())));
                if (imovel.getVisitasSize() > 20) {
                    System.out.println("Clientes com desconto: "
                            + (clienteFunc.findClienteByEmail((imovel.getVisitaById(imovel.getId_Visita() - 1)).getEmail_Cliente())).toString()
                            + "\n" + (clienteFunc.findClienteByEmail((imovel.getVisitaById(imovel.getId_Visita() - 2)).getEmail_Cliente())).toString());
                }
            }
        } else {
            throw new ElementNotFoundException("Não existe");
        }
    }

    /**
     *
     * Método usado para procurar pelo cliente
     *
     * @throws ElementNotFoundException
     */
    public void procurarCliente(Cliente clienteRecebido) throws ElementNotFoundException {
        boolean contains = clienteFunc.contains(clienteRecebido);

        if (contains) {
            Cliente cliente = clienteFunc.find(clienteRecebido);
            System.out.println(cliente.toString());
        } else {
            throw new ElementNotFoundException("Não existe");
        }
    }

    /**
     *
     * Método usado para criar a ligação entre o imóvel e o cliente
     *
     * @throws ElementNotFoundException, UncomparableException
     */
    public void adicionarVisita(int id_I, String email_C) throws ElementNotFoundException, UncomparableException {
        Imovel imovel = imovelFunc.findImovelById(id_I);
        if (imovelFunc.contains(imovel) && clienteFunc.contains(clienteFunc.findClienteByEmail(email_C))) {
            imovel.addVisita(id_I, email_C);
            if (imovel.getVisitasSize() > 20) {
                (clienteFunc.findClienteByEmail((imovel.getVisitaById(imovel.getId_Visita() - 3)).getEmail_Cliente())).setDesconto(1);
                (clienteFunc.findClienteByEmail((imovel.getVisitaById(imovel.getId_Visita() - 1)).getEmail_Cliente())).setDesconto(0.95);
                (clienteFunc.findClienteByEmail((imovel.getVisitaById(imovel.getId_Visita() - 2)).getEmail_Cliente())).setDesconto(0.95);
            }
        } else {
            throw new ElementNotFoundException("Erro no linear search.");
        }
    }

    /**
     *
     * Método usado para chamar os métodos de guardar dados para a imobiliária
     *
     * @throws UncomparableException
     */
    public void saveDataOption() {
        imovelFunc.saveData("imoveis");
        clienteFunc.saveData("clientes");
    }

    /**
     *
     * Método usado para chamar os métodos de carregar dados para a imobiliária
     *
     * @throws UncomparableException
     */
    public void loadDataOption() throws UncomparableException {
        loadImoveisData("imoveis");
        loadClientesData("clientes");
    }

    /**
     *
     * Método carregar os dados dos imóveis na imobiliária
     *
     * @throws UncomparableException
     */
    public void loadImoveisData(String fileName) throws UncomparableException {
        String fullFileName = fileName + ".txt";

        try {
            FileInputStream file = new FileInputStream(new File(fullFileName));
            ObjectInputStream oi = new ObjectInputStream(file);
            Object imovel = null;

            while ((imovel = oi.readObject()) != null) {
                imovelFunc.add((Imovel) imovel);
            }

            oi.close();
            file.close();
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado.");
        } catch (IOException e) {
            System.out.print("");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Método para carregar dados dos clientes na imobiliária
     *
     * @throws UncomparableException
     */
    public void loadClientesData(String fileName) throws UncomparableException {
        String fullFileName = fileName + ".txt";

        try {
            FileInputStream file = new FileInputStream(new File(fullFileName));
            ObjectInputStream oi = new ObjectInputStream(file);
            Object cliente = null;

            while ((cliente = oi.readObject()) != null) {
                clienteFunc.add((Cliente) cliente);
            }

            oi.close();
            file.close();
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado.");
        } catch (IOException e) {
            System.out.print("");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public class Imobiliaria {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ElementNotFoundException, EmptyCollectionException, UncomparableException, IOException {
        Funcionalidades funcionalidades = new Funcionalidades();
        funcionalidades.loadDataOption();
        int opcao;

        do {
            Scanner scanner = new Scanner(System.in);
            System.out.println(" ----------------------MENU----------------------- ");
            System.out.println("| 1 - Registar Imovel                             |");
            System.out.println("| 2 - Remover Imovel                              |");
            System.out.println("| 3 - Procurar Imovel                             |");
            System.out.println("| 4 - Apresentar todos os imoveis                 |");
            System.out.println("| 5 - Registar Cliente                            |");
            System.out.println("| 6 - Remover Cliente                             |");
            System.out.println("| 7 - Procurar Cliente                            |");
            System.out.println("| 8 - Criar visita                                |");
            System.out.println("| 9 - Apresentar todos os clientes                |");
            System.out.println("| 10 - Guardar dados                              |");
            System.out.println("| 11 - Carregar dados                             |");
            System.out.println("| 0 - Sair do programa                            |");
            System.out.println(" -------------------------------------------------");
            opcao = scanner.nextInt();

            switch (opcao) {
                case 1:
                    menuRegistoImovel(funcionalidades);
                    break;
                case 2:
                    menuRemoverImovel(funcionalidades);
                    break;
                case 3:
                    menuProcurarImovel(funcionalidades);
                    break;
                case 4:
                    funcionalidades.verificarImoveis();
                    break;
                case 5:
                    menuRegistoCliente(funcionalidades);
                    break;
                case 6:
                    menuRemoverCliente(funcionalidades);
                    break;
                case 7:
                    menuProcurarCliente(funcionalidades);
                    break;
                case 8:
                    menuCriarVisita(funcionalidades);
                    break;
                case 9:
                    funcionalidades.verificarClientes();
                    break;
                case 10:
                    funcionalidades.saveDataOption();
                    break;
                case 11:
                    funcionalidades.loadDataOption();
                    break;
                case 0:
                    sair();
                    break;
                default:
                    System.out.println("Selecione outra opção");
            }
        } while (opcao != 0);

    }

    public static void menuRegistoImovel(Funcionalidades funcionalidades) throws UncomparableException {
        Scanner scanner = new Scanner(System.in);

        System.out.println(" ------MENU DE REGISTO------ ");
        System.out.println("|       Insira o tipo       |");
        System.out.println(" --------------------------- ");
        String tipo = scanner.nextLine();

        System.out.println(" ------MENU DE REGISTO------ ");
        System.out.println("|    Insira a localidade    |");
        System.out.println(" --------------------------- ");
        String localidade = scanner.nextLine();

        System.out.println(" ------MENU DE REGISTO------ ");
        System.out.println("|       Insira o preço      |");
        System.out.println(" --------------------------- ");
        double preco = scanner.nextDouble();

        funcionalidades.adicionarImovel(tipo, localidade, preco);
    }

    public static void menuRegistoCliente(Funcionalidades funcionalidades) throws UncomparableException {
        Scanner scanner = new Scanner(System.in);

        System.out.println(" ------MENU DE REGISTO------ ");
        System.out.println("|       Insira o nome       |");
        System.out.println(" --------------------------- ");
        String nome = scanner.nextLine();

        System.out.println(" ------MENU DE REGISTO------ ");
        System.out.println("|      Insira o email       |");
        System.out.println(" --------------------------- ");
        String email = scanner.nextLine();

        System.out.println(" ------MENU DE REGISTO------ ");
        System.out.println("|     Insira o contacto     |");
        System.out.println(" --------------------------- ");
        String contacto = scanner.nextLine();

        Cliente newCliente = new Cliente(nome, email, contacto);

        funcionalidades.adicionarCliente(newCliente);
    }

    public static void menuRemoverImovel(Funcionalidades funcionalidades) throws EmptyCollectionException, ElementNotFoundException {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println(" ------MENU DE REMOÇÃO------ ");
        System.out.println("|       Insira o tipo       |");
        System.out.println(" --------------------------- ");
        String tipo = scanner.nextLine();

        System.out.println(" ------MENU DE REMOÇÃO------ ");
        System.out.println("|    Insira a localidade    |");
        System.out.println(" --------------------------- ");
        String localidade = scanner.nextLine();

        System.out.println(" ------MENU DE REMOÇÃO------ ");
        System.out.println("|       Insira o preço      |");
        System.out.println(" --------------------------- ");
        double preco = scanner.nextDouble();

        System.out.println(" ------MENU DE PROCURA------ ");
        System.out.println("|       Insira o id         |");
        System.out.println(" --------------------------- ");
        int id = scanner.nextInt();
        
        Imovel imovel = new Imovel(id, tipo, localidade, preco);

        funcionalidades.removerImovel(imovel);
    }

    public static void menuRemoverCliente(Funcionalidades funcionalidades) throws EmptyCollectionException, ElementNotFoundException {
        Scanner scanner = new Scanner(System.in);

        System.out.println(" ------MENU DE REMOÇÃO------ ");
        System.out.println("|       Insira o nome       |");
        System.out.println(" --------------------------- ");
        String nome = scanner.nextLine();

        System.out.println(" ------MENU DE REMOÇÃO------ ");
        System.out.println("|      Insira o email       |");
        System.out.println(" --------------------------- ");
        String email = scanner.nextLine();

        System.out.println(" ------MENU DE REMOÇÃO------ ");
        System.out.println("|     Insira o contacto     |");
        System.out.println(" --------------------------- ");
        String contacto = scanner.nextLine();

        Cliente cliente = new Cliente(nome, email, contacto);

        funcionalidades.removerCliente(cliente);
    }

    public static void menuProcurarImovel(Funcionalidades funcionalidades) throws ElementNotFoundException {
        Scanner scanner = new Scanner(System.in);

        System.out.println(" ------MENU DE PROCURA------ ");
        System.out.println("|       Insira o tipo       |");
        System.out.println(" --------------------------- ");
        String tipo = scanner.nextLine();

        System.out.println(" ------MENU DE PROCURA------ ");
        System.out.println("|    Insira a localidade    |");
        System.out.println(" --------------------------- ");
        String localidade = scanner.nextLine();

        System.out.println(" ------MENU DE PROCURA------ ");
        System.out.println("|       Insira o pre�o      |");
        System.out.println(" --------------------------- ");
        double preco = scanner.nextDouble();

        System.out.println(" ------MENU DE PROCURA------ ");
        System.out.println("|       Insira o id         |");
        System.out.println(" --------------------------- ");
        int id = scanner.nextInt();

        Imovel imovel = new Imovel(id, tipo, localidade, preco);

        funcionalidades.procurarImovel(imovel);
    }

    public static void menuProcurarCliente(Funcionalidades funcionalidades) throws ElementNotFoundException {
        Scanner scanner = new Scanner(System.in);

        System.out.println(" ------MENU DE PROCURA------ ");
        System.out.println("|       Insira o nome       |");
        System.out.println(" --------------------------- ");
        String nome = scanner.nextLine();

        System.out.println(" ------MENU DE PROCURA------ ");
        System.out.println("|      Insira o email       |");
        System.out.println(" --------------------------- ");
        String email = scanner.nextLine();

        System.out.println(" ------MENU DE PROCURA------ ");
        System.out.println("|     Insira o contacto     |");
        System.out.println(" --------------------------- ");
        String contacto = scanner.nextLine();

        Cliente cliente = new Cliente(nome, email, contacto);

        funcionalidades.procurarCliente(cliente);
    }

    private static void menuCriarVisita(Funcionalidades funcionalidades) throws ElementNotFoundException, UncomparableException {
        Scanner scanner = new Scanner(System.in);

        System.out.println(" -------MENU DE VISITA------- ");
        System.out.println(" ----------CLIENTE----------- ");
        System.out.println(" --------------------------- ");
        System.out.println("|     Insira o email do     |");
        System.out.println("|          cliente          |");
        System.out.println(" --------------------------- ");
        String email_C = scanner.nextLine();

        System.out.println(" ---------IMOBILIARIA-------- ");
        System.out.println("|   Insira a referencia da   |");
        System.out.println("|         imobiliaria        |");
        System.out.println(" ---------------------------- ");
        int id_I = scanner.nextInt();

        funcionalidades.adicionarVisita(id_I, email_C);
    }

    public static void sair() {
        System.out.println(" ---------------------------------------------- ");
        System.out.println("|      OBRIGADO POR USAR O NOSSO PROGRAMA      |");
        System.out.println(" ----------------------------------------------");

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public class SearchingLinkedList {

    /**
     * Método para pesquisar um elemento na lista de modo linear
     *
     * @return Devolve o elemento pesquisado ou erro caso não tenha sido encontrado
     * @throws ElementNotFoundException se o elemento não tiver sido encontrado
     */
    public static <T> T linearSearch(LinkedNode<T> head, T target) throws ElementNotFoundException {
        LinkedNode<T> index = head;

        boolean found = false;

        while (!found && index != null) {
            if (index.getElement().equals(target)) {
                found = true;
            } else {
                index = index.getNext();
            }
        }

        if (found == true) {
            return index.getElement();
        } else {
            throw new ElementNotFoundException("Erro no linear search.");
        }
    }

    /**
     * Método para pesquisar um imóvel na lista pelo id de modo linear
     *
     * @return Devolve o elemento pesquisado ou erro caso não tenha sido encontrado
     * @throws ElementNotFoundException se o elemento não tiver sido encontrado
     */
    public static <T> T linearSearchImovelById(LinkedNode<T> head, int id) throws ElementNotFoundException {
        LinkedNode<T> index = head;

        boolean found = false;

        while (!found && index != null) {
            if (((Imovel) (index.getElement())).getId() == id) {
                found = true;
            } else {
                index = index.getNext();
            }
        }

        if (found == true) {
            return index.getElement();
        } else {
            throw new ElementNotFoundException("Erro no linear search.");
        }
    }

    /**
     * Método para pesquisar um cliente na lista pelo email de modo linear
     *
     * @return Devolve o elemento pesquisado ou erro caso não tenha sido encontrado
     * @throws ElementNotFoundException se o elemento não tiver sido encontrado
     */
    public static <T> T linearSearchClienteByEmail(LinkedNode<T> head, String str) throws ElementNotFoundException {
        LinkedNode<T> index = head;

        boolean found = false;

        while (!found && index != null) {
            if (((Cliente) (index.getElement())).getEmail().equals(str)) {
                found = true;
            } else {
                index = index.getNext();
            }
        }

        if (found == true) {
            return index.getElement();
        } else {
            throw new ElementNotFoundException("Erro no linear search.");
        }
    }

    /**
     * Método para pesquisar uma visita na pelo id lista de modo linear
     *
     * @return Devolve o elemento pesquisado ou erro caso não tenha sido encontrado
     * @throws ElementNotFoundException se o elemento não tiver sido encontrado
     */
    public static <T> T linearSearchVisitaById(LinkedNode<T> head, int id) throws ElementNotFoundException {
        LinkedNode<T> index = head;

        boolean found = false;

        while (!found && index != null) {
            if (((Visita) (index.getElement())).getId() == id) {
                found = true;
            } else {
                index = index.getNext();
            }
        }

        if (found == true) {
            return index.getElement();
        } else {
            throw new ElementNotFoundException("Erro no linear search.");
        }
    }

    
    public static <T> String[] linearSearchGetArrayEmailCliente(LinkedNode<T> head, int size) throws ElementNotFoundException {
        LinkedNode<T> index = head;
        String[] array = new String[size];
        int i = 0;

        while (index != null) {
            array[i++] = ((Visita) (index.getElement())).getEmail_Cliente();
            index = index.getNext();
        }

        if (i == 0) {
            throw new ElementNotFoundException("Erro no linear search.");
        }
        return array;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public class BasicIterator<T> implements Iterator<T> {

    private LinkedNode<T> current;

    /**
     * 
     * Construtor
     * 
     */
    public BasicIterator(LinkedNode<T> head) {
        current = head;
    }

    /**
     * 
     * Método para verificar se existem mais elementos na lista
     *
     * @return retorna true caso haja mais elementos e falso caso o elemento
     * seja o último
     */
    @Override
    public boolean hasNext() {
        return current != null;
    }

    /**
     * 
     * Método para receber os próximo elemento
     *
     * @return retorna o elemento
     */
    @Override
    public T next() {
        if (current == null) {
            throw new NoSuchElementException("Iterator");
        }
        T element = current.getElement();
        current = current.getNext();
        return element;
    }

}

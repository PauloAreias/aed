/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

import java.io.Serializable;

/**
 *
 * @author Pedro Marques
 */
public class Imovel implements Comparable, Serializable {

    private OrderedLinkedList<Visita> visitasFunc = new OrderedLinkedList<>();

    private int id;
    private String tipo; //Casa, Apartamento, moradia,
    private String localidade;
    private double preco;
    private int id_Visita = 0;

    /**
     *
     * Construtor
     *
     */
    public Imovel() {
    }

    /**
     *
     * Construtor
     *
     */
    public Imovel(int id, String tipo, String localidade, double preco) {
        this.id = id;
        this.tipo = tipo;
        this.localidade = localidade;
        this.preco = preco;
    }

    /**
     *
     * Método get para receber o id
     *
     */
    public int getId() {
        return id;
    }

    /**
     *
     * Método set para alterar o id
     *
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * Método get para receber o tipo
     *
     */
    public String getTipo() {
        return tipo;
    }

    /**
     *
     * Método set para alterar o tipo
     *
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     *
     * Método get para receber a localidade
     *
     */
    public String getLocalidade() {
        return localidade;
    }

    /**
     *
     * Método get para alterar a localidade
     *
     */
    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    /**
     *
     * Método get para receber o preço
     *
     */
    public double getPreco() {
        return preco;
    }

    /**
     *
     * Método set para alterar o preço
     *
     */
    public void setPreco(double preco) {
        this.preco = preco;
    }

    /**
     *
     * Método get para receber o id da visita
     *
     */
    public int getId_Visita() {
        return id_Visita;
    }

    /**
     *
     * Método set para alterar o id da visita
     *
     */
    public void setId_Visita(int id_Visita) {
        this.id_Visita = id_Visita;
    }

    /**
     *
     * Método usado para adicionar uma visita ao imóvel
     *
     * @throws UncomparableException caso não seja possível comparar os
     * elementos
     */
    public void addVisita(int id_I, String email_C) throws UncomparableException {
        Visita visita = new Visita(id_Visita++, id_I, email_C);
        System.out.println(visita.toString());
        visitasFunc.add(visita);
    }

    /**
     *
     * Método usado para encontrar o tamanho das visitas
     *
     */
    public int getVisitasSize() {
        return visitasFunc.size();
    }

    /**
     *
     * Método usado para encontrar as visitas por id
     *
     * @throws ElementNotFoundException caso não seja possível encontrar o
     * elemento
     */
    public Visita getVisitaById(int id) throws ElementNotFoundException {
        return visitasFunc.findVisitaById(id);
    }

    public String[] getArrayEmailCliente() throws ElementNotFoundException {

        return visitasFunc.getArrayEmailCliente(visitasFunc.size());
    }

    /**
     *
     * Método usado para comparar dois elementos
     * @return Devolve 0 se os clientes forem iguais
     */
    @Override
    public int compareTo(Object object) {
        return id - (((Imovel) object).id);
    }

    /**
     *
     * Método usado para comparar dois elementos
     * @return Devolve true se os elementos forem iguais 
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Imovel)) {
            return false;
        }
        if ((Imovel) object == this) {
            return true;
        }

        Imovel imovel = (Imovel) object;
        if (!imovel.tipo.equals(this.tipo)) {
            return false;
        }
        if (!imovel.localidade.equals(this.localidade)) {
            return false;
        }
        if (imovel.preco != this.preco) {
            return false;
        }
        if (imovel.id != this.id) {
            return false;
        }
        return true;
    }

    /**
     *
     * Método usado para enviar uma string com os dados do imóvel
     * @return Devolve a string com os dados do imóvel
     */
    @Override
    public String toString() {
        return "Imovel: \nID do Imóvel: " + id + "\nTipo: " + tipo + ";\nLocalidade: " + localidade + ";\nPreço: " + preco + ";";
    }
}

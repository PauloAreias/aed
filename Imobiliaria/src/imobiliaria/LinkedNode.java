/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imobiliaria;

/**
 *
 * @author Pedro Marques & Paulo Areias
 */
public class LinkedNode<T> {

    private T element;
    private LinkedNode<T> next;

    /**
     * 
     * Construtor
     * 
     */
    public LinkedNode(T elem) {
        this.element = elem;
        this.next = null;
    }

    /**
     * Método get para receber a variável element
     *
     * @return Devolve o elemento
     */
    public T getElement() {
        return element;
    }

    /**
     * Método set para alterar a variável element
     */
    public void setElement(T element) {
        this.element = element;
    }

    /**
     * Método get para receber a variável next
     *
     * @return Devolve o elemento
     */
    public LinkedNode<T> getNext() {
        return next;
    }

    /**
     * Método get para alterar a variável next
     */
    public void setNext(LinkedNode<T> next) {
        this.next = next;
    }
}
